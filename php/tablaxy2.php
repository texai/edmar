<style>
	body table{
		font-size: 12px;
		text-align: center;
	}

	td {
		width: 25px;
		height: 25px;
	}
	td.cabeza{background-color: MistyRose;}
	td.igual{background-color: Orange ;}
	td.m5{background-color: LightSalmon ;}
	td.m7{background-color: YellowGreen ;}
	td.m35{background-color: DodgerBlue  ;}


</style>

<form>
	<input type="type" name="x" placeholder="escribe x"> <br>
	<input type="type" name="y" placeholder="escribe Y"> <br>
	<input type="submit" value="Generar Tabla">
</form>

<? if($_GET['x']!="" AND $_GET['y']!="" ): ?>

	<?php
	$x = $_GET['x'];
	$y = $_GET['y'];
	?>

	<table border="1">
		<tr>
			<td>@</td>
			<? foreach(range(1, $y) as $yi): ?>
				<td class="cabeza"><?=$yi?></td>
			<? endforeach; ?>
		</tr>
		<? foreach (range(1, $x) as $xi): ?>
		    <tr>
		    	<td class="cabeza"><?=$xi?></td>
				<? foreach(range(1, $y) as $yi): ?>
					<?php 
					$c = (($xi-1)*$y+$yi);
					if($xi==$yi){
						$clase = "igual";
					}elseif($c%35==0){
						$clase = "m35";
					}elseif($c%7==0){
						$clase = "m7";
					}elseif($c%5==0){
						$clase = "m5";
					}else{
						$clase = "";
					}
					?>
					<td class="<?=$clase?>">
						<?=$c?>
					</td>
				<? endforeach; ?>
		    </tr>
		<? endforeach; ?>
	</table>
<? endif; ?>
