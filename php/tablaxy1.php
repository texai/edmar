

<form>
	<input type="type" name="x" placeholder="escribe x"> <br>
	<input type="type" name="y" placeholder="escribe Y"> <br>
	<input type="submit" value="Generar Tabla">
</form>

<? if($_GET['x']!="" AND $_GET['y']!="" ): ?>

	<?php
	$x = $_GET['x'];
	$y = $_GET['y'];
	?>

	<table border="1">
		<? foreach (range(1, $x) as $xi): ?>
		    <tr>
				<? foreach(range(1, $y) as $yi): ?>
					<td>
						<? if($xi==$yi): ?>
							<b>==</b>
						<? elseif($xi*2==$yi): ?>
							<b>2x</b>
						<? else: ?>
							<?= (($xi-1)*$y+$yi)?>
						<?endif;?>
					</td>
				<? endforeach; ?>
		    </tr>
		<? endforeach; ?>
	</table>
<? endif; ?>
